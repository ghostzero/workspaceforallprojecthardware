#line 1 "C:/workSpace/forPicMikroC/triac/pic16f877a/1/base.c"






unsigned char FlagReg;
unsigned char Count;
unsigned char TotalCount;
unsigned char nCount;
unsigned char nTotalCount;
sbit ZC at FlagReg.B0;

void interrupt(){
 if (INTCON.INTF){
 ZC = 1;
 INTCON.INTF = 0;
 }
}

void main() {
 PORTB = 0;
 TRISB = 0x01;
 PORTA = 0;
 ADCON1 = 7;
 TRISA = 0xFF;
 PORTD = 0;
 TRISD = 0;
 OPTION_REG.INTEDG = 0;
 INTCON.INTF = 0;
 INTCON.INTE = 1;
 INTCON.GIE = 1;

 Count = 6;
 TotalCount = 10;
 nCount = Count;
 nTotalCount = TotalCount;

 while (1){
 if (ZC){
 if (nCount > 0){
 PORTD.B0 = 1;
 delay_us(250);
 PORTD.B0 = 0;
 nCount--;
 }
 nTotalCount--;
 if (nTotalCount == 0){
 nTotalCount = TotalCount;
 nCount = Count;
 }
 ZC = 0;
 }
 }
}
