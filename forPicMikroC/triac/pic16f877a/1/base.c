//---------------------------------------------------------------------------------------------------------
//Programmer: Syed Tahmid Mahbub
//Compiler: mikroC PRO for PIC v4.60
//Target PIC: PIC16F877A
//Program for pulse skipping modulation or cycle control
//---------------------------------------------------------------------------------------------------------
unsigned char FlagReg;
unsigned char Count;
unsigned char TotalCount;
unsigned char nCount;
unsigned char nTotalCount;
sbit ZC at FlagReg.B0;

void interrupt(){
     if (INTCON.INTF){          //INTF flag raised, so external interrupt occured
        ZC = 1;
        INTCON.INTF = 0;
     }
}

void main() {
     PORTB = 0;
     TRISB = 0x01;              //RB0 input for interrupt
     PORTA = 0;
     ADCON1 = 7;
     TRISA = 0xFF;
     PORTD = 0;
     TRISD = 0;                 //PORTD all output
     OPTION_REG.INTEDG = 0;      //interrupt on falling edge
     INTCON.INTF = 0;           //clear interrupt flag
     INTCON.INTE = 1;           //enable external interrupt
     INTCON.GIE = 1;            //enable global interrupt

     Count = 6;                 //number of half-cycles to be on
     TotalCount = 10;           //total number of half-cycles control
     nCount = Count;
     nTotalCount = TotalCount;

     while (1){
           if (ZC){ //zero crossing occurred
              if (nCount > 0){
                 PORTD.B0 = 1;
                 delay_us(250);
                 PORTD.B0 = 0;
                 nCount--;
              }
              nTotalCount--;
              if (nTotalCount == 0){ //number of required half-cycles elapsed
                 nTotalCount = TotalCount;
                 nCount = Count;
              }
              ZC = 0;
           }
     }
}
