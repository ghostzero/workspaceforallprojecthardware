
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;base.c,14 :: 		void interrupt(){
;base.c,15 :: 		if (INTCON.INTF){          //INTF flag raised, so external interrupt occured
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;base.c,16 :: 		ZC = 1;
	BSF        _FlagReg+0, 0
;base.c,17 :: 		INTCON.INTF = 0;
	BCF        INTCON+0, 1
;base.c,18 :: 		}
L_interrupt0:
;base.c,19 :: 		}
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;base.c,21 :: 		void main() {
;base.c,22 :: 		PORTB = 0;
	CLRF       PORTB+0
;base.c,23 :: 		TRISB = 0x01;              //RB0 input for interrupt
	MOVLW      1
	MOVWF      TRISB+0
;base.c,24 :: 		PORTA = 0;
	CLRF       PORTA+0
;base.c,25 :: 		ADCON1 = 7;
	MOVLW      7
	MOVWF      ADCON1+0
;base.c,26 :: 		TRISA = 0xFF;
	MOVLW      255
	MOVWF      TRISA+0
;base.c,27 :: 		PORTD = 0;
	CLRF       PORTD+0
;base.c,28 :: 		TRISD = 0;                 //PORTD all output
	CLRF       TRISD+0
;base.c,29 :: 		OPTION_REG.INTEDG = 0;      //interrupt on falling edge
	BCF        OPTION_REG+0, 6
;base.c,30 :: 		INTCON.INTF = 0;           //clear interrupt flag
	BCF        INTCON+0, 1
;base.c,31 :: 		INTCON.INTE = 1;           //enable external interrupt
	BSF        INTCON+0, 4
;base.c,32 :: 		INTCON.GIE = 1;            //enable global interrupt
	BSF        INTCON+0, 7
;base.c,34 :: 		Count = 6;                 //number of half-cycles to be on
	MOVLW      6
	MOVWF      _Count+0
;base.c,35 :: 		TotalCount = 10;           //total number of half-cycles control
	MOVLW      10
	MOVWF      _TotalCount+0
;base.c,36 :: 		nCount = Count;
	MOVLW      6
	MOVWF      _nCount+0
;base.c,37 :: 		nTotalCount = TotalCount;
	MOVLW      10
	MOVWF      _nTotalCount+0
;base.c,39 :: 		while (1){
L_main1:
;base.c,40 :: 		if (ZC){ //zero crossing occurred
	BTFSS      _FlagReg+0, 0
	GOTO       L_main3
;base.c,41 :: 		if (nCount > 0){
	MOVF       _nCount+0, 0
	SUBLW      0
	BTFSC      STATUS+0, 0
	GOTO       L_main4
;base.c,42 :: 		PORTD.B0 = 1;
	BSF        PORTD+0, 0
;base.c,43 :: 		delay_us(250);
	MOVLW      83
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
;base.c,44 :: 		PORTD.B0 = 0;
	BCF        PORTD+0, 0
;base.c,45 :: 		nCount--;
	DECF       _nCount+0, 1
;base.c,46 :: 		}
L_main4:
;base.c,47 :: 		nTotalCount--;
	DECF       _nTotalCount+0, 1
;base.c,48 :: 		if (nTotalCount == 0){ //number of required half-cycles elapsed
	MOVF       _nTotalCount+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main6
;base.c,49 :: 		nTotalCount = TotalCount;
	MOVF       _TotalCount+0, 0
	MOVWF      _nTotalCount+0
;base.c,50 :: 		nCount = Count;
	MOVF       _Count+0, 0
	MOVWF      _nCount+0
;base.c,51 :: 		}
L_main6:
;base.c,52 :: 		ZC = 0;
	BCF        _FlagReg+0, 0
;base.c,53 :: 		}
L_main3:
;base.c,54 :: 		}
	GOTO       L_main1
;base.c,55 :: 		}
	GOTO       $+0
; end of _main
